At Axis Health, Our goal is to help you achieve the best possible life in the present and future by providing excellent health care at an affordable price. We work hard to get you back to the best you can be as quickly as possible. Call us today! (541) 471-0397.

Address: 333 SW 5th Street, (corner of 5th and I St.), Grants Pass, OR 97526, USA
Phone: 541-471-0397
